package com.example.placesexample;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

/**
 * A basic loader to return a T
 * 
 * @param <T>
 */
public abstract class BaseLoader<T> extends AsyncTaskLoader<T> {

	private T result; 
	
	public BaseLoader(Context context) {
		super(context);
	}
	
	protected void onStartLoading() {
		if(result != null) {
			deliverResult(result);
		} else {
			forceLoad();
		}
	}

	@Override
	public T loadInBackground() {
		result = getResult(); 
		return result; 
	}
	
	public abstract T getResult(); 
}