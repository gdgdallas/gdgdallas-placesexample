package com.example.placesexample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.gson.Gson;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.text.Html;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends FragmentActivity implements LoaderCallbacks<PlaceResult>{
	
	TextView text1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		text1 = (TextView)findViewById(R.id.text1);
		getSupportLoaderManager().initLoader(0, null, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public Loader<PlaceResult> onCreateLoader(int arg0, Bundle arg1) {
		return new BaseLoader<PlaceResult>(this) {
			@Override
			public PlaceResult getResult() {
				try{
					return getPlaces(); 
				} catch (Exception ex) {
					ex.printStackTrace();
					return null;
				}
				
				
			}

			private PlaceResult getPlaces() throws IOException  {
				URL url = new URL("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=32.8410231,-97.1924821&sensor=false&radius=50000&keyword=starbucks&key=AIzaSyAUBMNk774FRfxbrWexGCJFnwSB17yDD6A");
				InputStream stream = (InputStream) url.getContent();
				String json = stream2string(stream);
				Gson gson = new Gson();
				PlaceResult result = gson.fromJson(json, PlaceResult.class);
				return result; 
			}
		};
	}

	@Override
	public void onLoadFinished(Loader<PlaceResult> arg0, PlaceResult arg1) {
		String out = "";
		for(Place place : arg1.results) {
			out += place.vicinity + "<br/>";
		}
		
		text1.setText(Html.fromHtml(out));
	}

	@Override
	public void onLoaderReset(Loader<PlaceResult> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public String stream2string(InputStream in) throws IOException {
		InputStreamReader is = new InputStreamReader(in);
		StringBuilder sb=new StringBuilder();
		BufferedReader br = new BufferedReader(is);
		String read = br.readLine();

		while(read != null) {
		    //System.out.println(read);
		    sb.append(read);
		    read =br.readLine();

		}

		return sb.toString();
	}


}
